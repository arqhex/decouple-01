<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';

    protected $fillable = [
        'username',
        'email',
        'password'       
    ];
}