<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Upvote extends Model {
    protected $table = 'upvote';
    protected $primaryKey = 'id_upvote';
    protected $fillable = [
        'id_answer',
        'id_usuario'
    ];
}