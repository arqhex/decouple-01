<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

class Question extends Model {

    protected $table = 'question';
    protected $primaryKey = 'id_question';
    protected $fillable = [
        'question',
        'id_usuario'
    ];

    public function answers(){
        return $this->hasMany('\Models\Answer', 'id_question');
    }    

}