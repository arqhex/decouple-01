<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

class Answer extends Model {

    protected $table = 'answer';
    protected $primaryKey = 'id_answer';
    protected $fillable = ['answer', 'id_usuario', 'id_question'];

}