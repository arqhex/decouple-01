<?php
namespace Controllers;

use Models\Answer;
use Models\Upvote;

class Answers {

    public static function add_answer($answer, $id_question, $id_user) {
        $answer = Answer::create([
            'answer' => $answer,
            'id_question' => $id_question,
            'id_usuario' => $id_user
        ]);
        return $answer;
    }

    public static function upvote_answer($id_answer, $id_usuario){
        $upvote = Upvote::create([
            'id_answer' => $id_answer,
            'id_usuario' => $id_usuario
        ]);
        return $upvote;
    }
}