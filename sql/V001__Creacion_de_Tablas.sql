CREATE TABLE usuario (

	id_usuario serial primary key,
	
	username varchar(100) DEFAULT NULL,
	email varchar(200) DEFAULT NULL,
	password varchar(200) DEFAULT NULL,
 
	created_at timestamp without time zone default now(),
	updated_at timestamp without time zone default NULL,
	deleted_at timestamp without time zone default NULL	
);

CREATE TABLE question (

	id_question serial primary key,
	
	question text,
	
	id_usuario integer REFERENCES usuario(id_usuario),
	
	created_at timestamp without time zone default now(),
	updated_at timestamp without time zone default NULL,
	deleted_at timestamp without time zone default NULL	
);

CREATE TABLE answer (

	id_answer serial primary key,
	
	answer text,
	
	id_usuario integer REFERENCES usuario(id_usuario),
	id_question integer REFERENCES question(id_question),
	
	created_at timestamp without time zone default now(),
	updated_at timestamp without time zone default NULL,
	deleted_at timestamp without time zone default NULL	
);

CREATE TABLE upvote (

	id_upvote serial primary key,
	
	id_answer integer REFERENCES answer(id_answer),
	id_usuario integer REFERENCES usuario(id_usuario),
 
	created_at timestamp without time zone default now(),
	updated_at timestamp without time zone default NULL,
	deleted_at timestamp without time zone default NULL	

);

